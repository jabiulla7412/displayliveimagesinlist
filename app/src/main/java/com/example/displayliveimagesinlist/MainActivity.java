package com.example.displayliveimagesinlist;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.example.displayliveimagesinlist.model.ImageDetails;
import com.example.displayliveimagesinlist.threds.FetchImagesBackGroundThread;
import com.example.displayliveimagesinlist.viewModel.ImagesListViewModel;

import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnScrollChangeListener,
                                                                SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG    = "JB_LOG_" + MainActivity.class.getName();

     private RecyclerView          mRvImageListView;
     private ImagesListViewModel   mImageListViewModel;
     private SwipeRefreshLayout    mSwipeRefresh;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRvImageListView = (RecyclerView) findViewById(R.id.rv_list_of_images);
        mRvImageListView.setHasFixedSize(true);

        mSwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.srl_swipe_refresh) ;
        mSwipeRefresh.setOnRefreshListener(this);
        mSwipeRefresh.setOnScrollChangeListener(this);

        mImageListViewModel = new ImagesListViewModel();
        mImageListViewModel.init(this, mRvImageListView);

        fetchImages();

    }


    @Override
    public void onRefresh() {
        fetchImages();
    }

    private void fetchImages(){
        FetchImagesBackGroundThread imagesBackGroundThread = new FetchImagesBackGroundThread(this,
                mImageListViewModel);
        imagesBackGroundThread.execute();
    }


    @Override
    public void onScrollChange(View view, int i, int i1, int i2, int i3) {
        fetchImages();
    }
}