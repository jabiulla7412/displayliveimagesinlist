package com.example.displayliveimagesinlist.exception;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;


public class WSException extends Exception {

    private static final String TAG = "JB_LOG_" + WSException.class;

    public WSException() {
    }

    public WSException(String pMessage) {
        super(pMessage);
        Log.d(TAG, "WSException: pMessage" + pMessage);
    }

    public WSException(Throwable pCause) {
        super(pCause);
        Log.d(TAG, "WSException: pCause" + pCause);
    }

    public WSException(final String pMessage, final Throwable pCause, final Context pContext) {
        super(pMessage, pCause);
        Log.d(TAG, "WSException: ClassName :: Message :: Cause ::" + pMessage + "::" + pCause);
    }

    public WSException(final String pMessage, final Throwable pCause) {
        super(pMessage, pCause);
        Log.d(TAG, "WSException: Message :: Cause ::" + pMessage + "::" + pCause);
    }
}


