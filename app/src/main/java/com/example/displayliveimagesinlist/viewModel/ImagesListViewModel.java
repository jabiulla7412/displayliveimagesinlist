package com.example.displayliveimagesinlist.viewModel;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.displayliveimagesinlist.R;
import com.example.displayliveimagesinlist.adapter.ImageAdapter;
import com.example.displayliveimagesinlist.model.ImageDetails;

import java.util.List;

public class ImagesListViewModel extends ViewModel {

    public static final String TAG    = "JB_LOG_" + ImagesListViewModel.class.getName();

    private List<ImageDetails>           mDataList;
    private Context                      mContext;

    private RecyclerView.Adapter         mAdapter;
    private RecyclerView.LayoutManager   mLayoutManager;
    private RecyclerView                 mRvImageList;

    public void init(Context pContext, RecyclerView pRecyclerView) {
        this.mContext = pContext;
        mLayoutManager = new LinearLayoutManager(pContext);
        pRecyclerView.setLayoutManager(mLayoutManager);
        mRvImageList = pRecyclerView;
    }

    public void setDataList(List<ImageDetails> pDataList) {
         mDataList = pDataList;
         mAdapter  = new ImageAdapter(mContext,mDataList);
         mRvImageList.setAdapter(mAdapter);
         mAdapter.notifyDataSetChanged();
      }

    public int getDataListSize(){
        return mDataList.size();
    }


    public ImageDetails getImageAt(Integer pIndex) {
        if (mDataList.size() != 0 &&
                pIndex != null ) {
            return mDataList.get(pIndex);
        }
        return null;
    }

}

