package com.example.displayliveimagesinlist.model;

import android.util.Log;

import com.example.displayliveimagesinlist.adapter.ImageAdapter;

import java.util.ArrayList;
import java.util.List;

public class ImageDetails {

    public static final String TAG    = "JB_LOG_" + ImageDetails.class.getName();

    private String id;
    private String author;
    private String height;
    private String url;
    private String downloadUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }


    public static List<ImageDetails> getStub() {
        List<ImageDetails> dataList = new ArrayList<>();
        for (int index = 0; index < 10; index++) {
            ImageDetails imageDetails = new ImageDetails();
            imageDetails.setAuthor("Author " + index);
            imageDetails.setId("0000 " + index);
            imageDetails.setUrl("http://url." + index);
            dataList.add(imageDetails);
        }
        return dataList;
    }

    public void displayImageObject(ImageDetails imageDetails){
        Log.d(TAG, "Author : " + imageDetails.getAuthor());
        Log.d(TAG, "DownloadURL : " + imageDetails.getDownloadUrl());
        Log.d(TAG, "Height   : " + imageDetails.getHeight());
        Log.d(TAG, "ID       : " + imageDetails.getId());
        Log.d(TAG, "URL : " + imageDetails.getUrl());

    }
}
