package com.example.displayliveimagesinlist.threds;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.displayliveimagesinlist.WebServices.api.FetchImagesAPI;
import com.example.displayliveimagesinlist.viewModel.ImagesListViewModel;

public class FetchImagesBackGroundThread extends AsyncTask<Void, Void, Void> {

    public static final String TAG    = "JB_LOG_" + FetchImagesBackGroundThread.class.getName();

    private Context             mContext;

    private ProgressDialog       mProgressDialog;
    private FetchImagesAPI       mFetchImagesAPI;
    private ImagesListViewModel  imagesListViewModel;

    public FetchImagesBackGroundThread(Context mContext, ImagesListViewModel pImageListViewModel){
        this.mContext = mContext;
        this.imagesListViewModel = pImageListViewModel;
    }
    @Override
    protected void onPreExecute() {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please Wait ...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mFetchImagesAPI = new FetchImagesAPI(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        mFetchImagesAPI.executeWebService();
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onPostExecute(Void aVoid) {
        Log.d(TAG, "onPostExecute: " + mFetchImagesAPI.getDataList().size() );
        imagesListViewModel.setDataList(mFetchImagesAPI.getDataList());
        mProgressDialog.dismiss();
    }
}