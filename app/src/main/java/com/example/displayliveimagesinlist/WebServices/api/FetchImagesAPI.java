package com.example.displayliveimagesinlist.WebServices.api;

import android.content.Context;
import android.media.Image;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.RequestFuture;
import com.example.displayliveimagesinlist.WebServices.ConnectToWebService;
import com.example.displayliveimagesinlist.WebServices.WebService;
import com.example.displayliveimagesinlist.model.ImageDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class FetchImagesAPI extends ConnectToWebService {

    public FetchImagesAPI(Context pContext) {
        super(pContext);
    }

    public FetchImagesAPI(Context pContext, int pSubscriberId) {
        super(pContext, pSubscriberId);
    }

    private List<ImageDetails> mDataList;

    @Override
    protected String getUrl() {
        return ConnectToWebService.FETCH_IMAGES;
    }

    @Override
    protected JSONObject prepareRequest() throws JSONException {
        return null;
    }

    @Override
    protected RequestFuture sendRequest(JSONObject request) {
        RequestFuture future          = RequestFuture.newFuture();
        JsonArrayRequest requestJson = new JsonArrayRequest(Request.Method.GET,
                ConnectToWebService.FETCH_IMAGES,
                null, future, future );
        requestJson.setRetryPolicy(mDefaultRetryPolicy);
        WebService.getInstance(mContext).addToRequestQueue(requestJson);
        return future;
    }

    @Override
    protected JSONArray receiveResponse(RequestFuture pRequestFuture) throws InterruptedException, ExecutionException, TimeoutException {
        return  (JSONArray) pRequestFuture.get(mTimeout, TimeUnit.SECONDS);
    }

    /*
    This approch is for limited number of rows ,
     If we have more rows will be using paging concept with limit and offset
     */
    @Override
    protected void processResponse(JSONArray pResponse) throws JSONException {
        Log.d(TAG, "processResponse: Response : " + pResponse);

        mDataList = new ArrayList<>();

        for (int index = 0; index < pResponse.length(); index++) {
             JSONObject json = pResponse.getJSONObject(index);

             ImageDetails imageDetailsBean = new ImageDetails();
             imageDetailsBean.setAuthor(json.getString("author"));
             imageDetailsBean.setUrl(json.getString("url"));
             imageDetailsBean.setDownloadUrl(json.getString("download_url"));
             imageDetailsBean.setHeight(json.getString("height"));
             imageDetailsBean.setId(json.getString("id"));

             mDataList.add(imageDetailsBean);
        }

    }

    public List<ImageDetails> getDataList() {
        return mDataList;
    }

}
