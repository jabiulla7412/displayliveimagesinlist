package com.example.displayliveimagesinlist.WebServices;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.example.displayliveimagesinlist.BuildConfig;
import com.example.displayliveimagesinlist.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class ConnectToWebService  {

    public static final String TAG    = "JB_LOG_" + ConnectToWebService.class.getName();

    protected Context   mContext ;

    public static final String URL = BuildConfig.SERVER_URL ;

    public static final String FETCH_IMAGES           = URL + "v2/list?page=2&limit=20";

    protected DefaultRetryPolicy mDefaultRetryPolicy;

    protected long               mTimeout             = 30;
    protected TimeUnit           mTimeoutUnit         = TimeUnit.SECONDS;

    protected int                mInitialTimeoutMills = 30*1000;
    protected int                mNumberOfRetries     = 3;
    protected float              mBackOffMultiplier   = 1.0F;

    protected String             mErrorMsg;
    protected boolean            mStatus;
    protected int                mErrorCode;


    public ConnectToWebService(Context pContext) {
         mContext = pContext;
        setDefaultRetryPolicy(mInitialTimeoutMills, mNumberOfRetries, mBackOffMultiplier);
        setDefaultTimeout(mTimeout, mTimeoutUnit);
    }

    public ConnectToWebService(Context pContext, int pSubscriberId) {
        mContext      = pContext;
        setDefaultRetryPolicy(mInitialTimeoutMills, mNumberOfRetries, mBackOffMultiplier);
        setDefaultTimeout(mTimeout, mTimeoutUnit);
    }

    protected void setDefaultRetryPolicy(int pInitialTimeoutMills, int pNumberOfRetries, float pBackOffMultiplier){
        mDefaultRetryPolicy = new DefaultRetryPolicy(pInitialTimeoutMills, pNumberOfRetries, pBackOffMultiplier);
    }

    protected void setDefaultTimeout(long pTimeout, TimeUnit pTimeoutUnit){
        mTimeout     = pTimeout;
        mTimeoutUnit = pTimeoutUnit;
    }

    public String getErrorMessage() {
        return mErrorMsg;
    }

    public void setErrorMessage(String mErrorMsg) {
        this.mErrorMsg = mErrorMsg;
    }

    public void setStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }

    public boolean isStatus(){
        return  mStatus;
    }

    public int  getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(int mErrorCode) {
        this.mErrorCode = mErrorCode;
    }

    abstract protected String getUrl();
    abstract protected JSONObject prepareRequest() throws JSONException;
    abstract protected RequestFuture sendRequest(JSONObject pRequest);
    abstract protected JSONArray receiveResponse(RequestFuture pRequestFuture) throws InterruptedException, ExecutionException, TimeoutException;
    abstract protected void processResponse(JSONArray pResponse) throws JSONException;

    public boolean executeWebService(){

        try {

            Log.d(TAG, "executeWebService: URL      : " + getUrl());
            RequestFuture requestFuture = sendRequest(null);
            JSONArray response = receiveResponse(requestFuture);
            Log.d(TAG, "executeWebService: RESPONSE : " + response);
            processResponse(response);

        } catch (InterruptedException e) {
            // send to server
           // sendError(e);
            mErrorMsg = e.toString();
            Log.d(TAG, "executeWebService: " + e.getMessage() + e.getCause() );
        } catch (ExecutionException e) {
            // send to server
            //sendError(e);
            mErrorMsg = e.toString();
            Log.d(TAG, "executeWebService: " + e.getMessage() + e.getCause() );
        } catch (TimeoutException e) {
            // send to server
           // sendError(e);
            mErrorMsg = e.toString();
            Log.d(TAG, "executeWebService: " + e.getMessage() + e.getCause() );
        } catch (JSONException e) {
            // send to server
           // sendError(e);
            mErrorMsg = e.toString();
            Log.d(TAG, "executeWebService: " + e.getMessage() + e.getCause() );
        }
        return false;
    }

}
