package com.example.displayliveimagesinlist.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.displayliveimagesinlist.MainActivity;
import com.example.displayliveimagesinlist.R;
import com.example.displayliveimagesinlist.model.ImageDetails;
import com.example.displayliveimagesinlist.viewModel.ImagesListViewModel;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    public static final String TAG    = "JB_LOG_" + ImageAdapter.class.getName();

    private List<ImageDetails>  mDataList;
    private Context             mContext;

    public ImageAdapter(Context pContext,List<ImageDetails> pDataList) {
        mDataList = pDataList;
        mContext  = pContext ;
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView  mTvImageAuthor;
        public ImageView mImageView;


        public ImageViewHolder(View pItemView) {
            super(pItemView);
            mTvImageAuthor = (TextView) pItemView.findViewById(R.id.tv_image_author);
            mImageView     = (ImageView) pItemView.findViewById(R.id.iv_display_image);
            mImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();

            ImageDetails imageDetails =  mDataList.get(position);
            String description  = "Id : " + imageDetails.getId() + "\n" +
                                   "Author : " + imageDetails.getAuthor() + "\n" +
                                    "Download URL : " + imageDetails.getDownloadUrl();

             switch (view.getId()){
                case R.id.iv_display_image : displayDescription(mContext,description);
                                            break;
            }

        }
    }



    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent,
                                              int viewType) {
        View itemLayout   = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_display_image,
                parent, false);
        ImageViewHolder viewHolder = new ImageViewHolder(itemLayout);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
          ImageDetails imageDetails = mDataList.get(position);
          holder.mTvImageAuthor.setText(imageDetails.getAuthor());
          displayImageAtPosition(imageDetails.getDownloadUrl(),holder.mImageView);
          Log.d(TAG, "onBindViewHolder: DownloadURL : " + imageDetails.getDownloadUrl());
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public  void displayImageAtPosition(String pUrl,ImageView pImageView){
        Log.d(TAG, "displayImageAtPosition: URL " + pUrl );
        Glide.with(mContext )
                .load( pUrl)
                .centerCrop()
                .dontAnimate()
                .into(pImageView);
    }

    private void displayDescription(final Context pContext, final String pMessage  ) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(pContext);

        builder.setMessage(pMessage)
                .setTitle("Description" )
                .setCancelable(true)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface pDialog, int pWhich) {
                        pDialog.dismiss();
                    }
                });

        AlertDialog lAlert = builder.create();
        lAlert.show();
    }

}
